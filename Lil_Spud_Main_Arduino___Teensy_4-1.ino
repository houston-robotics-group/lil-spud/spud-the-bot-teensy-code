
const double SOFTWARE_VERSION = 0.1;

//----------------------------------------------------------------------------------------------
// ::Pins

// Pin 13 has the LED on Teensy 4.1
const int LED_BUILTIN_TEENSY = 13;

// PWM Frequency is set to match PWM of 500 Hz on Arduino Uno
const int PWM_FREQUENCY_HZ = 500;

const int LEFT_MOTOR_PWM_OUT_PIN = 0;

const int RIGHT_MOTOR_PWM_OUT_PIN = 1;

int pirSensorInput = 0;
const int PIN_PIR_SENSOR_INPUT = 23;


int freqHzOfBuzz = 0;
int timeMsToBuzz = 0;
const int PIN_BUZZER_OUTPUT = 22;

// end ::Pins
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// ::Robot Dimensions

const int WHEEL_DIAMETER_INCHES = 10;
const int WHEEL_CIRCUMFERENCE_INCHES = 31.41593;
const int WHEEL_SECONDS_PER_REVOLUTION = 7;

const int QUARTER_REVOLUTION_TIME_MS = 1563;

const int SQUARE_SIDE_LENGTH_INCHES = 36;

// end ::Robot Dimensions
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// ::Sabertooth 2X12 Notes
/*
 * Product Code: TE-091-212
 * Brand: Dimension Engineering
 * 
 * How to set DIP Switch:
 * 
 * 1  OFF   R/C Mode = Switch 1 DOWN, with Switch 2 UP
 * 2  ON    R/C Mode = Switch 1 DOWN, with Switch 2 UP
 * 3  ON    Running lead-acid batteries. (OFF enables Lithium cutoff voltage for lithium batteries)\
 * 4  OFF   Independent Mode ( S1 conrols Motor 1A& Motor 1B, S2 control Motor 2A & 2B)
 * 5  ON    Linear Respone
 * 6  OFF   Microcontroller Mode enabled. Also disables Timeout Failsafe and auto-calibration (this sucks because I would like Timeout Failsafe)
 * 
 * Speed Control:
 * 
 * 1.0 millisecond pulses = Reverse Full Speed
 * 1.5 millisecond pulses = Stopped
 * 2.0 millisecond pulses = Forward Full Speed
 * 
 */
// end ::Sabertooth 2X12 Notes
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// ::PIR Sensor Notes
/*
 * It is NOT Arduino, we think it is Elegoo.
 * 
 * Link to Instructables:
 * https://www.instructables.com/How-to-Use-a-PIR-Motion-Sensor-With-Arduino/
 * 
 * With connector at bottom, Pin 0 is far left:
 * 
 * Pin 0  V+
 * Pin 1  Output
 * Pin 2  GND
 * 
 * Top Left Pot is Time Delay Adjust
 * Top Right Pot is Sensitivity Adjust
 * 
 * Single Trigger Mode:
 *    Set Jumper to L. 
 *    Time trigger is started immediately upon detecting motion. Continued detection is
 *    blocked.
 *    
 * Repeatable Trigger Mode:
 *    Set Jumper to H. 
 *    Time trigger is restarted everytime motion is detected.
 *    
 * For Lil Spud Spinning Demo in Ion Robot Corral, Set Trigger Mode to Repeatable using Jumper.
 * 
 */
// end ::PIR Sensor Notes
//----------------------------------------------------------------------------------------------

//----------------------------------------------------------------------------------------------
// ::Frequency Output 
// 500 Hz is PWM output

    // 2 milliseconds if at 254 (forward, but have to use 254)
    // 1 millisecond if at 127 (reverse, full speed)

int performLilDance = 0;

// the setup routine runs once when you press reset:
void setup() {

  Serial.begin(9600); // set up Serial library at 9600 bps
  
  Serial.print("Lil Spud Teensy 4.1 Software V"); Serial.println(SOFTWARE_VERSION);

  // initialize the digital pin as an output.
  pinMode(LED_BUILTIN_TEENSY, OUTPUT);

  // Left Motor Pin
  pinMode(LEFT_MOTOR_PWM_OUT_PIN, OUTPUT);
  analogWriteFrequency(LEFT_MOTOR_PWM_OUT_PIN, PWM_FREQUENCY_HZ); // Teensy 4.1 ~ Changes freq for 0, 42, 43

  // Right Motor Pin
  pinMode(RIGHT_MOTOR_PWM_OUT_PIN, OUTPUT);
  analogWriteFrequency(RIGHT_MOTOR_PWM_OUT_PIN, PWM_FREQUENCY_HZ); // Teensy 4.1 ~ Changes freq for Pins 1, 44, 45


  // PIR Sensor
  pinMode(PIN_PIR_SENSOR_INPUT, INPUT);

  // Buzzer
  pinMode(PIN_BUZZER_OUTPUT, OUTPUT);

  // Wait at startup to allow for operator to move away from Lil Spud
  Serial.println("Waiting for 30 seconds to allow operator to move away from Lil Spud.");
  delay(30000); // 30 seconds
  Serial.println("Now I'm awake, make me dance.");

  // Audible frequencies are between 2000 and 5000 Hz
  freqHzOfBuzz = 2000;
  timeMsToBuzz = 5000;
  tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
  delay(timeMsToBuzz); // Tone works in background need delay for same duration
  
}

// the loop routine runs over and over again forever:
void loop() {

  pirSensorInput = digitalRead(PIN_PIR_SENSOR_INPUT);  

  // Perform lil dance if movement detected
  if (pirSensorInput == 1) {
    digitalWrite(LED_BUILTIN_TEENSY, HIGH);
    performLilDance = HIGH;
  }
  // Do NOT perform lil dance if movement not detected
  else {
    digitalWrite(LED_BUILTIN_TEENSY, LOW);
    performLilDance = LOW;
  }

  if (performLilDance == HIGH) {

    performLilDance = LOW;

    Serial.println("Starting Lil Dance");

    // -----------------------------------------------
    // Do starting lil dance song
    //
    // Audible frequencies are between 2000 and 5000 Hz
    
    freqHzOfBuzz = 2000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion
    
    freqHzOfBuzz = 4000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion

    freqHzOfBuzz = 3000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion

    freqHzOfBuzz = 2000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion

    freqHzOfBuzz = 5000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion

    freqHzOfBuzz = 4000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion

    freqHzOfBuzz = 2000;
    timeMsToBuzz = 1000;
    tone(PIN_BUZZER_OUTPUT, freqHzOfBuzz, timeMsToBuzz);
    delay(timeMsToBuzz); // Tone works in background. Need delay of same duration to wait for tone completion
    
    // Do starting lil dance song
    // -----------------------------------------------

    digitalWrite(LED_BUILTIN_TEENSY, HIGH);   // turn the LED on (HIGH is the voltage level)

    // Turn a quarter revolution CW 0 to 90 deg
    Serial.println("Turn a quarter revolution CW 0 to 90 deg");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 172);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    Serial.println("Stopping to show off my welds for a second.");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CCW 90 to 0 deg
    Serial.println("Turn a quarter revolution CCW 90 to 0 deg");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 210);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 172);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    Serial.println("Stopping to show off my wheels for a second.");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CW 0 to 90 deg
    Serial.println("Turn a quarter revolution CW 0 to 90 deg");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 172);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    Serial.println("Stopping to stare down my enemies for a second.");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CCW 90 to 0 deg
    Serial.println("Turn a quarter revolution CCW 90 to 0 deg");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 210);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 172);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop
    Serial.println("Stopping.");
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);

    /* //DEBUG HSS// Uncomment code below to full 360 deg spin
    // Turn a quarter revolution CCW 0 to 90 deg
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 149);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CCW 90 to 180 deg
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 149);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CCW 180 to 270 deg
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 149);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop for a second
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    delay(1000);

    // Turn a quarter revolution CCW 270 to 360(0) deg
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 149);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 210);
    delay(QUARTER_REVOLUTION_TIME_MS);

    // Stop forever
    analogWrite(LEFT_MOTOR_PWM_OUT_PIN, 191);
    analogWrite(RIGHT_MOTOR_PWM_OUT_PIN, 191);
    *///DEBUG HSS// Uncomment code below to full 360 deg spin

    Serial.print("Done with Lil Dance. Wait 30 seconds to unlock another dance.... performLilDance="); Serial.println(performLilDance);

    digitalWrite(LED_BUILTIN_TEENSY, LOW);   // turn the LED on (HIGH is the voltage level)

    // Wait at end of dance to prevent performing Lil Dance for the delay
    delay(30000); // 30 seconds

    Serial.println("Done with wait period. I will now dance for you again (;");
  
  }
  
}
